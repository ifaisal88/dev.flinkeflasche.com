<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property Collection|BillingDetail[] $billing_details
 * @property Collection|ShippingDetail[] $shipping_details
 * @property Collection|State[] $states
 *
 * @package App\Models
 */
class Country extends Model
{
	use SoftDeletes;
	protected $table = 'countries';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function billing_details()
	{
		return $this->hasMany(BillingDetail::class);
	}

	public function shipping_details()
	{
		return $this->hasMany(ShippingDetail::class);
	}

	public function states()
	{
		return $this->hasMany(State::class);
	}
}
