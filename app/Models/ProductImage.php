<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductImage
 *
 * @property int $id
 * @property int $product_id
 * @property bool $is_thumbnail
 * @property string|null $path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 *
 * @property Product $product
 *
 * @package App\Models
 */
class ProductImage extends Model
{
	use SoftDeletes;
	protected $table = 'product_images';

	protected $casts = [
		'product_id' => 'int',
		'is_thumbnail' => 'bool',
		'status' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'name',
		'size',
		'extension',
		'status'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
