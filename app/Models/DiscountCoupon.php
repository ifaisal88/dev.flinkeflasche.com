<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DiscountCoupon
 * 
 * @property int $id
 * @property int $availed_by_user_id
 * @property string $name
 * @property float|null $discount_percentage
 * @property float|null $discount_amount
 * @property string|null $order_number
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property User $user
 * @property Collection|ShoppingCart[] $shopping_carts
 *
 * @package App\Models
 */
class DiscountCoupon extends Model
{
	use SoftDeletes;
	protected $table = 'discount_coupons';

	protected $casts = [
		'availed_by_user_id' => 'int',
		'discount_percentage' => 'float',
		'discount_amount' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'availed_by_user_id',
		'name',
		'discount_percentage',
		'discount_amount',
		'order_number',
		'status'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'availed_by_user_id');
	}

	public function shopping_carts()
	{
		return $this->hasMany(ShoppingCart::class, 'coupon_id');
	}
}
