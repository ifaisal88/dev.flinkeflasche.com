<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShoppingCart
 * 
 * @property int $id
 * @property int $user_id
 * @property int $coupon_id
 * @property int $payment_method_id
 * @property string $order_number
 * @property int $total_quantity
 * @property float $total_price
 * @property float $total_tax
 * @property float $total_discount
 * @property float $shipping_price
 * @property float $deposit
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property DiscountCoupon $discount_coupon
 * @property PaymentMethod $payment_method
 * @property User $user
 *
 * @package App\Models
 */
class ShoppingCart extends Model
{
	use SoftDeletes;
	protected $table = 'shopping_carts';

	protected $casts = [
		'user_id' => 'int',
		'coupon_id' => 'int',
		'payment_method_id' => 'int',
		'total_quantity' => 'int',
		'total_price' => 'float',
		'total_tax' => 'float',
		'total_discount' => 'float',
		'shipping_price' => 'float',
		'deposit' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'user_id',
		'coupon_id',
		'payment_method_id',
		'order_number',
		'total_quantity',
		'total_price',
		'total_tax',
		'total_discount',
		'shipping_price',
		'deposit',
		'status'
	];

	public function discount_coupon()
	{
		return $this->belongsTo(DiscountCoupon::class, 'coupon_id');
	}

	public function payment_method()
	{
		return $this->belongsTo(PaymentMethod::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
