<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductReview
 * 
 * @property int $id
 * @property int $product_id
 * @property int $parent_id
 * @property int $user_id
 * @property string $reviews
 * @property float $ratings
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property ProductReview $product_review
 * @property Product $product
 * @property User $user
 * @property Collection|ProductReview[] $product_reviews
 *
 * @package App\Models
 */
class ProductReview extends Model
{
	use SoftDeletes;
	protected $table = 'product_reviews';

	protected $casts = [
		'product_id' => 'int',
		'parent_id' => 'int',
		'user_id' => 'int',
		'ratings' => 'float',
		'status' => 'bool'
	];

	protected $fillable = [
		'product_id',
		'parent_id',
		'user_id',
		'reviews',
		'ratings',
		'status'
	];

	public function product_review()
	{
		return $this->belongsTo(ProductReview::class, 'parent_id');
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function product_reviews()
	{
		return $this->hasMany(ProductReview::class, 'parent_id');
	}
}
