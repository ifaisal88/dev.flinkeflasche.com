<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethod
 * 
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $status
 * 
 * @property Collection|ShoppingCart[] $shopping_carts
 *
 * @package App\Models
 */
class PaymentMethod extends Model
{
	use SoftDeletes;
	protected $table = 'payment_methods';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function shopping_carts()
	{
		return $this->hasMany(ShoppingCart::class);
	}
}
