<!-- Header -->
<header class="fixed-top">
    <div class="TopHeadBar">
      <div class="container">
        <div class="row">
          <div class="col-2">
            <div class="form-check form-switch pt-1">
              <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
              <label class="form-check-label labelPrivate toggleCustomer" for="flexSwitchCheckDefault">Private Customers</label>
              <label class="form-check-label labelBusiness toggleCustomer" for="flexSwitchCheckDefault" style="display: none;">Bussiness Customers</label>
            </div>
          </div>
          <div class="col-5">
            <div class="searchbar">
              <input class="search_input" type="search" placeholder="Search...">
                <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="col-2 pt-2 text-center">
            <a href="wishlist.html" class="mobileCart"> Wishlist <i class="fas fa-heart"></i></a>
            <a href="#" class="mobileCart">
              <i class="fas fa-shopping-cart"  type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                  <!-- Number of Product in Cart -->
                  <span class="num-cart-product" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">0</span>
              </i>
              </a>
          </div>
          <div class="col-3 text-center"  id="logIn">
            <a href="login.html">CREATE A NEW CUSTOMER ACCOUNT</a>
          </div>
        </div>
      </div>
    </div>
    <div class="SeacondHead">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <div class="col-8 mx-auto row">
            <div class="col-auto">
                <a href="{{ route('home') }}"><img class="logo" src="{{ asset('assets/images/logo.png') }}"/></a>
            </div>
            <!-- <div class=""> -->
                <button class="navbar-toggler col-auto ms-auto m-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            <!-- </div> -->
            <!-- <div class="col"> -->
                <div class="collapse navbar-collapse col-auto" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto mb-3">
                    <li class="nav-item dropdown">
                        <button href="{{ route('category', ['id' => 1]) }}" class="nav-link dropdown-toggle mainItem" id="beerDropdown" aria-current="page" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="ture">
                        <img src="{{ asset('assets/images/menu-images/beer-mug.png') }}"/> <br> Beers
                        </button>
                        <div class="dropdown-menu" aria-labelledby="beerDropdown">
                        <a class="dropdown-item" href="#">Non-alcoholic beers</a>
                        <a class="dropdown-item" href="#">Beer mix drinks</a>
                        <a class="dropdown-item" href="#">Warehouse & bright</a>
                        <a class="dropdown-item" href="#">Malt beers</a>
                        <a class="dropdown-item" href="#">Pilsen beers</a>
                        <a class="dropdown-item" href="#">Wheat beers</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem"  href="{{ route('category', ['id' => 2]) }}" id="wineDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/wine.png') }}"/> <br> Sparkling wine & wines
                        </button>
                        <div class="dropdown-menu" aria-labelledby="wineDropdown">
                        <a class="dropdown-item" href="#">Rosé wines</a>
                        <a class="dropdown-item" href="#">Red wines</a>
                        <a class="dropdown-item" href="#">Sparkling wine & Prosecco</a>
                        <a class="dropdown-item" href="#">White wines</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem" href="{{ route('category', ['id' => 3]) }}" id="waterDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/water.png') }}"/> <br> Mineral Water
                        </button>
                        <div class="dropdown-menu" aria-labelledby="waterDropdown">
                        <a class="dropdown-item" href="#">Medicinal water</a>
                        <a class="dropdown-item" href="#">very carbonated (classic)</a>
                        <a class="dropdown-item" href="#">low-carbonation (medium)</a>
                        <a class="dropdown-item" href="#">non-carbonated (natural)</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem" href="{{ route('category', ['id' => 4]) }}" id="juiceDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/fruit-juice.png') }}"/> <br/>Juices
                        </button>
                        <div class="dropdown-menu" aria-labelledby="juiceDropdown">
                        <a class="dropdown-item" href="#">Organic juices</a>
                        <a class="dropdown-item" href="#">Not-from-concentrate juices</a>
                        <a class="dropdown-item" href="#">Fruit juice drinks</a>
                        <a class="dropdown-item" href="#">Nectars</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem" href="{{ route('category', ['id' => 5]) }}" id="sodaDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/soft-drinks.png') }}"/> <br/>Sodas
                        </button>
                        <div class="dropdown-menu" aria-labelledby="sodaDropdown">
                        <a class="dropdown-item" href="#">Cola drinks</a>
                        <a class="dropdown-item" href="#">Mate drinks</a>
                        <a class="dropdown-item" href="#">Spritzers</a>
                        <a class="dropdown-item" href="#">Other lemonades</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem" href="{{ route('category', ['id' => 6]) }}" id="miscDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/milk-box.png') }}"/> <br/> Miscellaneous
                        </button>
                        <div class="dropdown-menu" aria-labelledby="miscDropdown">
                        <a class="dropdown-item" href="#">Office supplies</a>
                        <a class="dropdown-item" href="#">Pastries</a>
                        <a class="dropdown-item" href="#">Coffee</a>
                        <a class="dropdown-item" href="#">Milk</a>
                        <a class="dropdown-item" href="#">Tea</a>
                        <a class="dropdown-item" href="#">Rental</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <button class="nav-link dropdown-toggle mainItem" href="catalog.html" id="OptionDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('assets/images/menu-images/media.png') }}"/> <br/> Catalog
                        </button>
                    </li>

                </ul>
            </div>
            <!-- </div> -->

            </div>
        </nav>
      </div>
    </header>
  <!-- Header -->
