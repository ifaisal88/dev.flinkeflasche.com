<!-- SideBar Part -->
<div class="col-md-3 col-sm-4">
    <div class="sideBar text-center mt-3">
        <img src="{{ asset('assets/images/logo.png') }}"/>
        <div class="mb-5">
            <h5 class="text-left mt-3">Product Filters</h5>
            <div class="accordion" id="accordionExample">
                <div class="card border-0">
                    <div class="p-0 cardBackground" id="headingOne">
                        <h2 class="mb-0">
                        <button class="col-12 mx-auto categoryBtnOne" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        All Products<span class="count">(1000)</span>
                        </button>
                        </h2>
                    </div>
                </div>
                <div class="card border-0">
                <div class="p-0 cardBackground" id="headingTwo">
                    <h2 class="mb-0">
                    <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Beers <span class="count">(153)</span><i class="fas fa-chevron-down categoryArrow"></i>
                    </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Non-alcoholic beers</a></li>
                            <li class="subCategory-list"><a href="#">Beer mix drinks</a></li>
                            <li class="subCategory-list"><a href="#">Warehouse & bright</a></li>
                            <li class="subCategory-list"><a href="#">Malt beers</a></li>
                            <li class="subCategory-list"><a href="#">Pilsen beers</a></li>
                            <li class="subCategory-list"><a href="#">Wheat beers</a></li>
                        </ul>
                    </div>
                </div>
                </div>
                <div class="card border-0">
                <div class="p-0 cardBackground" id="headingThree">
                    <h2 class="mb-0">
                    <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Sodas <span class="count">(153)</span><i class="fas fa-chevron-down categoryArrow"></i>
                    </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Cola drinks</a></li>
                            <li class="subCategory-list"><a href="#">Mate drinks</a></li>
                            <li class="subCategory-list"><a href="#">Spritzers</a></li>
                            <li class="subCategory-list"><a href="#">Other lemonades</a></li>
                        </ul>
                    </div>
                </div>
                </div>
                <div class="card border-0">
                    <div class="p-0 cardBackground" id="headingFour">
                    <h2 class="mb-0">
                        <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Mineral Water <span class="count">(103)</span><i class="fas fa-chevron-down categoryArrow"></i>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Medicinal water</a></li>
                            <li class="subCategory-list"><a href="#">very carbonated (classic)</a></li>
                            <li class="subCategory-list"><a href="#">low-carbonation (medium)</a></li>
                            <li class="subCategory-list"><a href="#">non-carbonated (natural)</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                <div class="card border-0">
                    <div class="p-0 cardBackground" id="headingFive">
                    <h2 class="mb-0">
                        <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Juices
                            <span class="count">(103)</span><i class="fas fa-chevron-down categoryArrow"></i>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Organic juices</a></li>
                            <li class="subCategory-list"><a href="#">Not-from-concentrate juices</a></li>
                            <li class="subCategory-list"><a href="#">Fruit juice drinks</a></li>
                            <li class="subCategory-list"><a href="#">Nectars</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                <div class="card border-0">
                    <div class="p-0 cardBackground" id="headingSix">
                    <h2 class="mb-0">
                        <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Sparkling wine & wines
                            <span class="count">(103)</span><i class="fas fa-chevron-down categoryArrow"></i>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Rosé wines</a></li>
                            <li class="subCategory-list"><a href="#">Red wines</a></li>
                            <li class="subCategory-list"><a href="#">Sparkling wine & Prosecco</a></li>
                            <li class="subCategory-list"><a href="#">White wines</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                <div class="card border-0">
                    <div class="p-0 cardBackground" id="headingSeven">
                    <h2 class="mb-0">
                        <button class="col-12 mx-auto categoryBtnOne collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            Miscellaneous
                            <span class="count">(103)</span><i class="fas fa-chevron-down categoryArrow"></i>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="subCategory">
                            <li class="subCategory-list"><a href="#">Office supplies</a></li>
                            <li class="subCategory-list"><a href="#">Pastries</a></li>
                            <li class="subCategory-list"><a href="#">Coffee</a></li>
                            <li class="subCategory-list"><a href="#">Milk</a></li>
                            <li class="subCategory-list"><a href="#">Tea</a></li>
                            <li class="subCategory-list"><a href="#">Rental</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SideBar Part -->
