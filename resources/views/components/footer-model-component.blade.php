<!-- Modal Area -->
        <!-- Header Basket Modal -->
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
            <div class="offcanvas-header">
              <h5 id="offcanvasRightLabel">Your Cart</h5>
              <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <div class="RightSideBar text-center">
                <div class="row">
                  <div class="col p-0">
                    <img src="images/product-category/beers/Beer mix drinks/Augustinians bright.jpg" style="max-width: 40px;"/>
                  </div>
                  <div class="col p-0">
                      <a href="#" class="RSideProductName">
                        Augustinians
                      </a>
                      <p class="AddcartPrice">1 × € 12.40</p>
                  </div>
                  <div class="col p-0">
                    <a href="#" class="RSideProductName"><i class="far fa-window-close"></i></a>
                  </div>
                </div>
              </div>
              <div class="SubTotal text-center">
                <div class="row">
                  <h3>Subtotal: € 10.42</h3>
                </div>
              </div>
              <div class="bottomTwoBtn text-center">
                <div class="row">
                  <div class="col p-0">
                    <a href="viewcart.html"><button class="BottomBtnOne">View Shopping Cart</button></a>
                  </div>
                  <div class="col p-0">
                    <button class="BottomBtnTwo">Cash Register</button>
                  </div>
                </div>
                <div class="row">
                  <p class="addToCartText">Please note that for technical reasons,
                    any additional costs (stair fee, mixed box or surcharge for small quantities)
                    are not included in the total.
                  </p>
                </div>
              </div>


            </div>
          </div>
        <!-- Header Basket Modal -->

        <!-- Add To Cart Basket OnClick Modal -->
        <div class="modal fade" id="AddToCartModal" tabindex="-1" aria-labelledby="AddToCartModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="AddToCartModalLabel">Product Add in Basket</h5>
                  <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body m-auto">
                   <button type="button" class="ViewBasketButton">View Basket</button>
                  </div>
              </div>
          </div>
        </div>
        <!-- Add To Cart Basket OnClick Modal -->
        <!-- QuickViewModal -->
          <div class="modal fade" id="QuickViewModal" tabindex="-1" aria-labelledby="QuickViewModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="QuickViewModalLabel">Allgäuer Büble Edelbräu</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <table class="m-auto container">
                            <tbody>
                              <div class="row">
                                <div class="col leftSide">
                                    <div class="quickViewItem">
                                        <img src="images/product-category/beers/Beer mix drinks/Augustinians bright.jpg">
                                    </div>
                                </div>
                                <div class="col rightSide">
                                  <p><h2><a href="#">Allgäuer Büble Edelbräu</a></h2></p>
                                  <p>€ 19.50</p>
                                  <div class="container p-0">
                                    <div class="plusminus">
                                        <input type="number" style="display: none;">
                                        <button class="cartBasket">
                                            <i aria-hidden="true" title="Add to Cart" class="fas fa-shopping-cart"></i>
                                        </button>
                                    </div>
                                  </div>
                                  <p class="pt-3"><a href="#">SKU number : 1012</a></p>
                                  <p><a href="#">Categories: Name</a></p>
                                  <p><a href="#">Radler source</a></p>
                                </div>
                              </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        <!-- QuickViewModal -->
        <!--WishList Modal  -->
          <div class="modal fade" id="WishListModal" tabindex="-1" aria-labelledby="WishListModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="WishListModalLabel">Product Add in Wishlist</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body m-auto">
                     <button type="button" class="ViewWishlistButton">View Wishlist</button>
                    </div>
                </div>
            </div>
          </div>
        <!--WishList Modal  -->
        <!-- CompareModal -->
          <div class="modal fade" id="CompareModal" tabindex="-1" aria-labelledby="CompareModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                      <div class="modal-header">
                      <h5 class="modal-title" id="CompareModalLabel">Modal title</h5>
                      <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body">
                          <table class="m-auto container">
                              <tbody>
                                  <tr class="TableRow">
                                      <th class="TableHeading"></th>
                                      <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                      <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                      <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                      <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading"></th>
                                      <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                      <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                      <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                      <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">TITLE</th>
                                      <td class="TableData">BioZisch Mate</td>
                                      <td class="TableData">Augustinians bright</td>
                                      <td class="TableData">Krombacher Radler</td>
                                      <td class="TableData">Bitburger Pils</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">Price</th>
                                      <td class="TableData">€ 12.40</td>
                                      <td class="TableData">€ 19.50</td>
                                      <td class="TableData">€ 16.60</td>
                                      <td class="TableData">€ 15.60</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">ADD TO CART</th>
                                      <td class="TableData"><button>Add to Cart</button></td>
                                      <td class="TableData"><button>Add to Cart</button></td>
                                      <td class="TableData"><button>Add to Cart</button></td>
                                      <td class="TableData"><button>Add to Cart</button></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">DESCRIPTION</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">SKU</th>
                                      <td class="TableData">1022</td>
                                      <td class="TableData">1508</td>
                                      <td class="TableData">1140</td>
                                      <td class="TableData">1104</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">AVAILABILITY</th>
                                      <td class="TableData">In stock</td>
                                      <td class="TableData">In stock</td>
                                      <td class="TableData">In stock</td>
                                      <td class="TableData">In stock</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading" >WEIGHT</th>
                                      <td class="TableData">-</td>
                                      <td class="TableData">-</td>
                                      <td class="TableData">-</td>
                                      <td class="TableData">-</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">DIMENSIONS</th>
                                      <td class="TableData">n / A</td>
                                      <td class="TableData">n / A</td>
                                      <td class="TableData">n / A</td>
                                      <td class="TableData">n / A</td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">MEASURING UNIT</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">SPARKLING</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">FIRE</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">BOTTLE TYPE</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th  class="TableHeading">BOTTLE SIZE</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading">DEGREE OF SWEETNESS</th>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                      <td class="TableData"></td>
                                  </tr>
                                  <tr class="TableRow">
                                      <th class="TableHeading"></th>
                                      <td class="TableData">€ 12.40</td>
                                      <td class="TableData">€ 19.50</td>
                                      <td class="TableData">€16.60</td>
                                      <td class="TableData">€ 15.60</td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
        <!-- CompareModel -->
      <!-- Modal Area -->
