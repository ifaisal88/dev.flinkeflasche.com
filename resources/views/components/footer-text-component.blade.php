  <!-- Main-Footer -->
  <section id="MainFooter">
    <div class="container">
      <div class="row">
        <div class="col-9">
          <ul class="footermenu">
            <li><a href="deliveryandtime.html">Delivery area and times</a></li>
            <li><a href="condition.html">Conditions</a></li>
            <li><a href="imprint.html">Imprint</a></li>
            <li><a href="paymentmethod.html">Payment Methods</a></li>
            <li><a href="privacypolicy.html">Privacy Policy</a></li>
            <li><a href="revocation.html">Revocation</a></li>
            <li><a href="https://dialog-mineralwasser.de/">Information about mineral water</a></li>
          </ul>
          <div id="endFooter">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <p id="copyRight" class="mb-0 mt-0">
                    Copyright 2021 © Nimble Bottle
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-3">
          <ul class="paymentIconList">
            <li class="paymentIcon">
              <img title="Cash on Delivery" src="{{ asset('assets/images/payment-icon/delivery-truck.png') }}"/>
            </li>
            <li class="paymentIcon">
              <img title="Bank Transfer" src="{{ asset('assets/images/payment-icon/symbols.png') }}"/>
            </li>
            <li class="paymentIcon">
              <img title="RECHNUNG" src="{{ asset('assets/images/payment-icon/invoice.png') }}"/>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
<!-- Main-Footer -->
