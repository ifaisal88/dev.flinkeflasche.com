<!-- Bootstrap Scripts -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/spinner.js') }}"></script>
<script src="{{ asset('assets/js/xzoom.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
<!-- Bootstrap Scripts -->
<!-- Owl - Carousel -->
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<!-- Owl - Carousel -->

<!-- Script Functions -->
<script>
  $(".form-switch input").click(function() {
    $(".labelPrivate, .labelBusiness").toggle();
  });
</script>
<script>
  $("input[type='number']").inputSpinner()
  // Search Bar---->
        // OnClick Open Search Bar
        $(document).on('click','.search', function(){
              $('.search-bar').addClass('search-bar-active')
          });
        // OnClick Open Search Bar

        // OnClick Close Search Bar
          $(document).on('click','.search-cancel', function(){
            $('.search-bar').removeClass('search-bar-active')
          });
        // OnClick Close Search Bar
      // for Search Bar---->
      // Main Slider
        $(document).ready(function(){
          $("#bannerSlider").owlCarousel({
            items:1,
            autoplay:true,
            loop: true,
            // nav:true,
            dots: true,
            animateOut: 'fadeOut',
            autoplayTimeout:2500,
            autoplayHoverPause:true,
          });
        });
      //Main Slider
      // Product-Slider
        $(document).ready(function() {
          $("#productSlider").owlCarousel({
            items : 4,
            loop:true,
            // autoplay:true,
            nav:false,
            autoplayTimeout:2500,
            autoplayHoverPause:true,
            responsive : {
              // breakpoint from 0 up
              0 : {
                  items:2
              },
              // breakpoint from 768 up
              900 : {
                  item:4
              }
            }
            // navText: true,
          });
        });
        // Slider ToolTips
          // tooltips
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
              var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
          // tooltips
        // Slider ToolTips
      // Product-slider
      //recommendation Slider
        $(document).ready(function() {
          $("#recommend").owlCarousel({
            items : 4,
            loop:true,
            autoplay:true,
            autoplayHoverPause:true,
            nav:false,
            autoplayTimeout:2500,
            responsive : {
              // breakpoint from 0 up
              0 : {
                  items:2
              },
              // breakpoint from 768 up
              900 : {
                  item:4
              }
            }
            // navText: true,
          });
        });
      //recommendation Slider
        // Menu category Click
        $(".mainItem").click(function(){
        var url = $(this).attr('href');
        window.location = url;
      })
    // Menu category Click
    </script>
<!-- Script Functions -->
