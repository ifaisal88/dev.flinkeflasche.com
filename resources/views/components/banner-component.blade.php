<!-- Section Slider -->
<section id="mainSLider">
    <div class="fullSlider container">
      <div id="bannerSlider" class="owl-carousel">
        <div> <img src="{{ asset('assets/images/Slides/bauerpausenzeit.jpg') }}"/></div>
        <div> <img src="{{ asset('assets/images/Slides/guestrower.jpg') }}"/></div>
        <div> <img src="{{ asset('assets/images/Slides/liebenwerder-sanft.jpg') }}"/></div>
        <div> <img src="{{ asset('assets/images/Slides/preussen.jpg') }}"/></div>
      </div>
    </div>
  </section>
  <!-- Section Slider -->
  <!-- Section Banner -->
    <Section id="Banner">
      <div class="container">
        <div class="row">
        <div class="col bannerImage">
          <img src="{{ asset('assets/images/Banner-icons/truck.png') }}"/>
          <p>Delivery Time</p>
        </div>
        <div class="col bannerImage">
          <img src="{{ asset('assets/images/Banner-icons/return.png') }}"/>
          <p>Easy return of the deposit</p>
        </div>
        <div class="col bannerImage">
          <img src="{{ asset('assets/images/Banner-icons/timer.png') }}"/>
          <p>Free shipping</p>
        </div>
      </div>
      </div>
    </Section>
  <!-- Sectino Banner -->
  <!-- Section Background-Image -->
    <section id="bgImage">
      <div class="container pt-5">
        <div class="row">
          <div class="col bgLogo">
            <img src="{{ asset('assets/images/logo.png') }}"/>
          </div>
          <div class="col text-center">
            <img class="bannerText" src="{{ asset('assets/images/Banner-icons/banner-text.png') }}"/>
          </div>
        </div>
      </div>
    </section>
  <!-- Section Background-Image -->
