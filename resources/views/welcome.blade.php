<x-header-component></x-header-component>
<x-header-menu-component></x-header-menu-component>
<x-banner-component></x-banner-component>
      <!-- Section BestSeller -->
        <section id="bestSeller">
          <h3>Best Seller</h3>
          <div id="productSlider" class="owl-carousel">
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>
                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
          </div>
        </section>
      <!-- Section BestSeller -->
      <!-- Section callToaction -->
        <section id="callToaction">
          <div class="container">
            <div class="row">
              <div class="callout text-center">
                <h1>This  - <b>is</b> - Heading </h1>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat ad assumenda nisi harum omnis itaque ea accusamus sint numquam nam est quod ab dolorum atque, quisquam exercitationem tempora similique! Corrupti.</p>
                <button>Click Here</button>
              </div>
            </div>
          </div>
        </section>
      <!-- Section callToaction -->
      <!-- Section three Pictures -->
        <section id="threePics" class="pt-5 pb-5">
          <div class="container p-0">
            <div class="col-12">
              <div class="row m-0">
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic1.jpg') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic2.jpg') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic3.png') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      <!-- Section three Pictures -->
      <!-- Section Our recommendation -->
      <section id="ourRecommend">
        <h3>Our Recommendation</h3>
          <div id="recommend" class="owl-carousel">
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
          </div>
      </section>
    <!-- Section recommendation -->

    <!-- Section Double-Banner -->
      <section id="DoubleBanner">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="doubleImg">
                <img src="{{ asset('assets/images/Banner-icons/double-banner.jpg') }}"/>
              </div>
            </div>
            <div class="col">
              <div class="double">
                  <div class="double-callout">
                    <h3 class="m-0 mt-3">This  - <b>is</b> - Heading </h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <button>Click Here</button>
                  </div>
                  <div class="ribbon">
                    <p>it's Ribbon</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    <!-- Section Double-Banner -->
    <!-- Section Message in bottle -->
      <section id="messageBottle">
        <h3>This is The Message in the Bottle</h3>
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="icon">
                <i class="far fa-clock fa-3x"></i>
                <h4>Delivery in 120 minutes</h4>
                <br>
                <p>We deliver within 120 minutes of placing your order. Always and at no extra charge! Monday to Saturday from 9 a.m. to 4 p.m.</p>
              </div>

            </div>
            <div class="col-sm-4">
              <div class="icon">
                <i class="fas fa-shuttle-van fa-3x"></i>
                <h4>Free shipping</h4>
                <br>
                <p>You will receive all orders free of charge at the door of your house or apartment. It doesn't matter which floor it is on.</p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="icon">
                <i class="far fa-money-bill-alt fa-3x"></i>
                <h4>Easy refund deposit</h4>
                <br>
                <p>We just take the empty goods back with us. The deposit amount will be deducted directly from the invoice on site.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    <!-- Section Message ibn bottle -->
    <!-- Section Brands -->
      <section id="footerBrands">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="fas fa-tags fa-3x"></i></p>
              </div>
              <h5><b>Fair prices</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="fas fa-wallet fa-3x"></i></p>
              </div>
              <h5><b>Pay safely</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i class="fas fa-shopping-cart fa-3x"></i></p>
              </div>
              <h5><b>Low minimum order<br>value</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="far fa-heart fa-3x"></i></p>
              </div>
              <h5><b>Convenient pre-order</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="far fa-thumbs-up fa-3x"></i></p>
              </div>
              <h5><b>Convenient pre-order</b></h5>
            </div>
          </div>
        </div>
      </section>
    <!-- Section Brands -->
    <x-footer-text-component></x-footer-text-component>
    <x-footer-model-component></x-footer-model-component>
    <x-footer-component></x-footer-component>
