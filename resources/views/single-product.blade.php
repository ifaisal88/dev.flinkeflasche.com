<x-header-component></x-header-component>
<x-header-menu-component></x-header-menu-component>
<!-- Main-Content -->
   <!-- default start -->

   <section id="default" class="padding-top0 ZoomPart SingleProduct">
     <div class="container">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-6 text-center">
            <div class="xzoom-container">
              <img class="xzoom" id="xzoom-default" src="{{ asset('assets/images/productrelated/Augustinians bright.jpg') }}" xoriginal="{{ asset('assets/images/productrelated/Augustinians bright-00.jpg') }}" />
              <div class="xzoom-thumbs">
                <a href="{{ asset('assets/images/productrelated/Augustinians bright-00.jpg') }}">
                    <img class="xzoom-gallery img-fluid" width="80" src="{{ asset('assets/images/productrelated/Augustinians bright-000.jpg') }}"  xpreview="{{ asset('assets/images/productrelated/Augustinians bright.jpg') }}">
                </a>
                <a href="{{ asset('assets/images/productrelated/productrelated-01.jpg') }}">
                    <img class="xzoom-gallery img-fluid" width="80" src="{{ asset('assets/images/productrelated/productrelated-111.jpg') }}">
                </a>
                <a href="{{ asset('assets/images/productrelated/productrelated-22.jpg') }}">
                    <img class="xzoom-gallery img-fluid" width="80" src="{{ asset('assets/images/productrelated/productrelated-02.jpg') }}">
                </a>
                <a href="{{ asset('assets/images/productrelated/productrelated-33.jpg') }}">
                    <img class="xzoom-gallery img-fluid" width="80" src="{{ asset('assets/images/productrelated/productrelated-03.jpg') }}">
                </a>
              </div>
            </div>
          </div>
          <div class="col-4 SingleProductDescription">
            <div class="SingleProductRight">
                <ul>
                    <li>ArtNr:{{ $product->item_number }}</li>
                    <li><h2>{{ $product->name }}</h2></li>
                    <li><h4>Augustinians bright</h4></li>
                    <li>12 X 1,0 | </li>
                    <li>PET - Mehrweg </li>
                </ul>
            </div>
            <div class="pt-3 col-12">
              <div class="container p-0">
                  <div class="plusminusSingleProduct">
                      <input type="number" style="display: none;">
                      <a>
                        <button class="singleproductcartBasket col-12">
                        <i aria-hidden="true" title="Add to Cart" class="fas fa-shopping-cart"></i>
                        In den Warenkorb
                        </button>
                      </a>
                  </div>
              </div>
            </div>
            <div class="col-12 SingleProductRightSide">
                <ul>
                  <li><h2>Lieferpreis<span class="categoryProductPrice">19.50 €</span></h2></li>
                  <li>zzgl. Pfand <span class="Pfand">3,30 €</span></li>
                  <li>inkl. 19% MwSt.</li>
                  <li>0,80 € pro Liter</li>
                </ul>
            </div>
            <ul class="singleProductIcons">
              <li><a href="#"><img class="img-fluid" src="{{ asset('assets/images/productrelated/productrelated-01.jpg') }}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{ asset('assets/images/productrelated/productrelated-02.jpg') }}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{ asset('assets/images/productrelated/productrelated-01.jpg') }}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{ asset('assets/images/productrelated/productrelated-02.jpg') }}" alt=""></a></li>
            </ul>
          </div>
        </div>
      </div>

    </section>
    <!-- Section Tabs -->
    <section id="DescriptionTabs">
      <div class="container">
          <div class="row">
              <ul class="nav nav-tabs p-0" id="myTab" role="tablist">
                  <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="item-tab" data-bs-toggle="tab" data-bs-target="#ITEM" type="button" role="tab" aria-controls="home" aria-selected="true">ITEM DESCRIPTION</button>
                  </li>
                  <li class="nav-item" role="presentation">
                  <button class="nav-link" id="ingredients-tab" data-bs-toggle="tab" data-bs-target="#INGREDIENTS" type="button" role="tab" aria-controls="profile" aria-selected="false">INGREDIENTS</button>
                  </li>
                  <li class="nav-item" role="presentation">
                  <button class="nav-link" id="manufacturer-tab" data-bs-toggle="tab" data-bs-target="#MANUFACTURER" type="button" role="tab" aria-controls="contact" aria-selected="false">MANUFACTURER DESCRIPTION</button>
                  </li>
                  <li class="nav-item" role="presentation">
                  <button class="nav-link" id="additional-tab" data-bs-toggle="tab" data-bs-target="#ADDITIONAL" type="button" role="tab" aria-controls="contact" aria-selected="false">ADDITIONAL INFORMATION</button>
                  </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="ITEM" role="tabpanel" aria-labelledby="item-tab">
                      <h3 class="pt-5">Item Description</h3>
                      <br/>
                      <p>{{ $product->description }}</p>
                  </div>
                  <div class="tab-pane fade" id="INGREDIENTS" role="tabpanel" aria-labelledby="ingredients-tab">
                      <h3 class="pt-5">Ingredients</h3>
                      <br/>
                      <p>{{ $product->ingredients }}</p>
                  </div>
                  <div class="tab-pane fade" id="MANUFACTURER" role="tabpanel" aria-labelledby="manufacturer-tab">
                      <h3 class="pt-5">Manufacturer description</h3>
                      <br/>
                      <p>Almost 400 years ago, the brewery from Neumarkt in the Upper Palatinate near Nuremberg was first mentioned in a document.
                          In 1800 it passed into the possession of the Ehrnsperger family.
                          From then on, several generations of the family personally and with a lot of passion brewed the Lammsbräu beers in Neumarkt in the Upper Palatinate near Nuremberg.
                      </p>
                      <br/>
                      <p>
                          When Dr. Franz Ehrnsperger took over the family business, a new chapter was opened: The brewery was converted to a completely sustainable corporate concept - which is still the pace for its actions today.
                      </p>
                      <br/>
                      <p>
                          “Organic” in this sense did not exist back then. Therefore, there were no raw materials that met the ecological standards of Neumarkter Lammsbräu. That is why the producers' group “Gold der Oberpfalz” was founded in 1978, laying the foundation for the supply of organic brewing raw materials.
                          The supplying farmers continued to convert their farms to the organic cultivation of brewing barley, wheat, spelled and natural hop cones.
                      </p>
                      <br/>
                      <p>
                          After a few years of ecological management, the basis for the first trial brews with the ecological raw materials was established. In the Nuremberg subsidiary - the brewery in the Altstadthof - the latest ecological brewing processes were tried out and the first organic beers were produced.
                      </p>
                      <br/>
                      <p>
                          It was not until a few years later that the first organic beers could be produced in larger batches. In 1987, the first organic beers, the draft beer and dark from the Neumarkter Lammsbräu, were introduced on the German market.
                      </p>
                      <br/>
                      <p>
                          It took a few years again until all raw materials fully met the ecological quality standards. In 1990, natural hop cones from one hundred percent organic farming were used for the first time. In 1994 the time had come: the beers at Neumarkter Lammsbräu are 100% organic.
                      </p>
                  </div>
                  <div class="tab-pane fade" id="ADDITIONAL" role="tabpanel" aria-labelledby="additional-tab">
                      <h3 class="pt-5">ADDITIONAL INFORMATION</h3>
                      <br/>
                      <p>brand <a href="#">Lamb brew</a></p>
                      <br/>
                      <p>Bottle type <a href="#">Glass - reusable</a></p>
                      <br/>
                      <p>Bottle size <a href="#">0.33 liters</a></p>
                  </div>
              </div>
          </div>
      </div>
  </section>
<!-- Section Tabs -->
<!-- default end -->
<!-- Main-Content -->

<x-footer-text-component></x-footer-text-component>
<x-footer-model-component></x-footer-model-component>
<x-footer-component></x-footer-component>
