<x-header-component></x-header-component>
<x-header-menu-component></x-header-menu-component>
<!-- Section Beers Main content -->
    <section id="beersContent">
        <div class="container-fluid">
            <div class="row">
                <x-sidebar-component></x-sidebar-component>
                <!-- Main Product Category Part -->
                <div class="col-md-9 col-sm-8">
                    <div class="mainProduct">
                        <div class="PageTitle">
                            <h2>Category: {{ $category->name }}</h2>
                        </div>
                        <!-- Category Single Product Part -->
                            <div class="container m-0">
                                <div class="row">
                                    @foreach ($products as $product)
                                    <div class="col-6 col-md-3 col-lg-3">
                                        <div class="row myDIV position-relative SingleOutline pt-4">
                                            <!-- Category Single Product Image Part -->
                                                <div class="boxImage col-4 p-0">
                                                    <a href="{{ route('singleproduct', $product->id) }}"><img class="productImage img-fluid" src="{{ 'http://dev.flinkeflasche.de/storage/products/' . $product->image }}" /></a>
                                                </div>
                                            <!-- Category Single Product Image Part -->

                                            <!-- Category Single Product Right Part -->
                                                <ul class="boxRightSide col-8">
                                                    <li><a href="{{ route('singleproduct', $product->id) }}"><img  title="Neu" class="brandImages" src="{{ asset('assets/images/brands/brand.png') }}"/></a></li>
                                                    <li><a href="{{ route('singleproduct', $product->id) }}"><p>Art No:<span>{{ $product->item_number }}</span></p></a></li>
                                                    <li><a class="brandName" href="{{ route('singleproduct', $product->id) }}">{{ $product->name }}</a></li>
                                                    <li><a class="productName" href="{{ route('singleproduct', $product->id) }}">{{ $product->category->name }}</a></li>
                                                    {{-- <li><a class="meg" href="{{ route('singleproduct', $product->id) }}">{{ $product->unit }} <span>|</span></a></li> --}}
                                                    <li><a class="petMeg" href="{{ route('singleproduct', $product->id) }}">PET - {{ $product->category->name }}</a></li>
                                                    <!--
                                                     -->
                                                    <li>
                                                        <div class="container p-0 mt-4">
                                                            <div class="PlusMinus">
                                                                <input type="number" style="display: none;">
                                                                <button class="cartBasket">
                                                                    <i aria-hidden="true" title="Add to Cart" class="fas fa-shopping-cart"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="singleproduct.html">Lieferpreis<span class="categoryProductPrice">{{ $product->customer_price }} €</span></a></li>

                                                    <!-- Category Single Product Part Customer Required Text -->
                                                    <div class="mt-1">
                                                        <li><a href="singleproduct.html">zzgl. Pfand <span class="Pfand">3,30 €</span></a></li>
                                                        <li><a href="singleproduct.html">{{ $product->vat }} MwSt.</a></li>
                                                        <li style="float: left;"><a href="singleproduct.html">€ 1.95 per liter</a></li>
                                                        <li style="float: right;">
                                                            <a class="Compare" href="#">
                                                                <i title="Compare" class="fas fa-sync-alt fa-lg"></i>
                                                            </a>
                                                            <a class="Wishlist" href="#">
                                                                <i title="Add to wishlist" class="far fa-heart fa-lg"></i>
                                                            </a>
                                                        </li>
                                                    </div>
                                                    <!-- Category Single Product Part Customer Required Text -->
                                                </ul>
                                            <div class="col-12 position-absolute itemBrandMax">
                                                <img src="" alt="" width="70%">
                                            </div>
                                            <div class="stripe position-absolute"
                                                style="height: 100%;
                                                width: 17px;
                                                z-index: 99999;
                                                background-color: #ffffff;
                                                left: -8px;
                                                top: 0px;
                                                border-radius: 5px;
                                                padding: 0;">
                                            </div>
                                            <ul class="singleProductCate" id="hide"style="
                                            position: absolute;
                                            left: -98px;
                                            border-radius: 15px 0px 0px 12px;
                                            top: 8px;
                                            list-style: none;
                                            width: 100px;
                                            /* margin: 0px; */
                                            background-color: #fff;
                                            z-index: 999;">
                                                <li><img src="{{ asset('assets/images/productrelated/productrelated-01.jpg') }}" class="itemBrand"></li>
                                                <li><img src="{{ asset('assets/images/productrelated/productrelated-02.jpg') }}" class="itemBrand"></li>
                                                <li><img src="{{ asset('assets/images/productrelated/productrelated-03.jpg') }}" class="itemBrand"></li>
                                                <li><img src="{{ asset('assets/images/productrelated/productrelated-03.jpg') }}" class="itemBrand"></li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        <!-- Category Single Product Part -->
                        <!-- Pagination Button-->
                        <div class="paginationButton">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item List">
                                    <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="page-item List"><a class="page-link" href="#">1</a></li>
                                <li class="page-item List"><a class="page-link" href="#">2</a></li>
                                <li class="page-item List"><a class="page-link" href="#">3</a></li>
                                <li class="page-item List">
                                    <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                <!-- Pagination Button -->
                    </div>
                </div>
                <!-- Main Product Category Part -->
            </div>
        </div>
    </section>
<!-- Section Beers Main content -->
    <x-footer-text-component></x-footer-text-component>
    <x-footer-model-component></x-footer-model-component>
    <x-footer-component></x-footer-component>
