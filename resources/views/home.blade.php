<x-header-component></x-header-component>
<x-header-menu-component></x-header-menu-component>
<x-banner-component></x-banner-component>
      <!-- Section BestSeller -->
        <section id="bestSeller">
          <h3>Best Seller</h3>
          <div id="productSlider" class="owl-carousel">
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>
                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
          </div>
        </section>
      <!-- Section BestSeller -->
      <!-- Section callToaction -->
        <section id="callToaction">
          <div class="container">
            <div class="row">
              <div class="callout text-center">
                <h1>This  - <b>is</b> - Heading </h1>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat ad assumenda nisi harum omnis itaque ea accusamus sint numquam nam est quod ab dolorum atque, quisquam exercitationem tempora similique! Corrupti.</p>
                <button>Click Here</button>
              </div>
            </div>
          </div>
        </section>
      <!-- Section callToaction -->
      <!-- Section three Pictures -->
        <section id="threePics" class="pt-5 pb-5">
          <div class="container p-0">
            <div class="col-12">
              <div class="row m-0">
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic1.jpg') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic2.jpg') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="outline">
                    <div class="outline2">
                      <img src="{{ asset('assets/images/pic3.png') }}" style="width: 100%;height: 320px;"/>
                    </div>
                    <div class="picText">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      <!-- Section three Pictures -->
      <!-- Section Our recommendation -->
      <section id="ourRecommend">
        <h3>Our Recommendation</h3>
          <div id="recommend" class="owl-carousel">
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
            <div class="single">
              <div class="singleProduct">
                <img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/>
                  <ul class="productBtn">
                    <li>
                      <button type="button"data-bs-toggle="modal" data-bs-target="#QuickViewModal">
                        <i id="viewProduct" class="fas fa-search-plus fa-lg"  data-bs-toggle="tooltip" data-bs-placement="left" title="Quick View" ></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#WishListModal">
                        <i id="viewProduct" class="far fa-heart fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Wishlist"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#CompareModal">
                        <i id="viewProduct" class="fas fa-sync fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Compare"></i>
                      </button>
                    </li>
                    <li>
                      <button type="button" data-bs-toggle="modal" data-bs-target="#AddToCartModal">
                        <i id="viewProduct" class="fas fa-shopping-bag fa-lg" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to Cart"></i>
                      </button>
                    </li>

                  </ul>
              </div>
              <p class="productCategory">
                <a href="#">juices</a>
              </p>
              <p class="productName">
                <a href="#">Allgäuer Büble Edelbräu</a>
              </p>
              <p class="productPrice">16,99€</p>
            </div>
          </div>
      </section>
    <!-- Section recommendation -->

    <!-- Section Double-Banner -->
      <section id="DoubleBanner">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="doubleImg">
                <img src="{{ asset('assets/images/Banner-icons/double-banner.jpg') }}"/>
              </div>
            </div>
            <div class="col">
              <div class="double">
                  <div class="double-callout">
                    <h3 class="m-0 mt-3">This  - <b>is</b> - Heading </h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <button>Click Here</button>
                  </div>
                  <div class="ribbon">
                    <p>it's Ribbon</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    <!-- Section Double-Banner -->
    <!-- Section Message in bottle -->
      <section id="messageBottle">
        <h3>This is The Message in the Bottle</h3>
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="icon">
                <i class="far fa-clock fa-3x"></i>
                <h4>Delivery in 120 minutes</h4>
                <br>
                <p>We deliver within 120 minutes of placing your order. Always and at no extra charge! Monday to Saturday from 9 a.m. to 4 p.m.</p>
              </div>

            </div>
            <div class="col-sm-4">
              <div class="icon">
                <i class="fas fa-shuttle-van fa-3x"></i>
                <h4>Free shipping</h4>
                <br>
                <p>You will receive all orders free of charge at the door of your house or apartment. It doesn't matter which floor it is on.</p>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="icon">
                <i class="far fa-money-bill-alt fa-3x"></i>
                <h4>Easy refund deposit</h4>
                <br>
                <p>We just take the empty goods back with us. The deposit amount will be deducted directly from the invoice on site.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    <!-- Section Message ibn bottle -->
    <!-- Section Brands -->
      <section id="footerBrands">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="fas fa-tags fa-3x"></i></p>
              </div>
              <h5><b>Fair prices</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="fas fa-wallet fa-3x"></i></p>
              </div>
              <h5><b>Pay safely</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i class="fas fa-shopping-cart fa-3x"></i></p>
              </div>
              <h5><b>Low minimum order<br>value</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="far fa-heart fa-3x"></i></p>
              </div>
              <h5><b>Convenient pre-order</b></h5>
            </div>
            <div class="col">
              <div class="lastIcons">
                <p><i aria-hidden="true" class="far fa-thumbs-up fa-3x"></i></p>
              </div>
              <h5><b>Convenient pre-order</b></h5>
            </div>

          </div>
        </div>
      </section>
    <!-- Section Brands -->
    <x-footer-text-component></x-footer-text-component>
     <!-- Modal Area -->
        <!-- Header Basket Modal -->
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
          <div class="offcanvas-header">
            <h5 id="offcanvasRightLabel">Your Cart</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <div class="offcanvas-body">
            <div class="RightSideBar text-center">
              <div class="row">
                <div class="col p-0">
                  <img src="images/product-category/beers/Beer mix drinks/Augustinians bright.jpg" style="max-width: 40px;"/>
                </div>
                <div class="col p-0">
                    <a href="#" class="RSideProductName">
                      Augustinians
                    </a>
                    <p class="AddcartPrice">1 × € 12.40</p>
                </div>
                <div class="col p-0">
                  <a href="#" class="RSideProductName"><i class="far fa-window-close"></i></a>
                </div>
              </div>
            </div>
            <div class="SubTotal text-center">
              <div class="row">
                <h3>Subtotal: € 10.42</h3>
              </div>
            </div>
            <div class="bottomTwoBtn text-center">
              <div class="row">
                <div class="col p-0">
                  <a href="viewcart.html"><button class="BottomBtnOne">View Shopping Cart</button></a>
                </div>
                <div class="col p-0">
                  <button class="BottomBtnTwo">Cash Register</button>
                </div>
              </div>
              <div class="row">
                <p class="addToCartText">Please note that for technical reasons,
                  any additional costs (stair fee, mixed box or surcharge for small quantities)
                  are not included in the total.
                </p>
              </div>
            </div>


          </div>
        </div>
      <!-- Header Basket Modal -->

      <!-- Add To Cart Basket OnClick Modal -->
      <div class="modal fade" id="AddToCartModal" tabindex="-1" aria-labelledby="AddToCartModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="AddToCartModalLabel">Product Add in Basket</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body m-auto">
                 <button type="button" class="ViewBasketButton">View Basket</button>
                </div>
            </div>
        </div>
      </div>
      <!-- Add To Cart Basket OnClick Modal -->
      <!-- QuickViewModal -->
        <div class="modal fade" id="QuickViewModal" tabindex="-1" aria-labelledby="QuickViewModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl">
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="QuickViewModalLabel">Allgäuer Büble Edelbräu</h5>
                  <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body">
                      <table class="m-auto container">
                          <tbody>
                            <div class="row">
                              <div class="col leftSide">
                                  <div class="quickViewItem">
                                      <img src="images/product-category/beers/Beer mix drinks/Augustinians bright.jpg">
                                  </div>
                              </div>
                              <div class="col rightSide">
                                <p><h2><a href="#">Allgäuer Büble Edelbräu</a></h2></p>
                                <p>€ 19.50</p>
                                <div class="container p-0">
                                  <div class="plusminus">
                                      <input type="number" style="display: none;">
                                      <button class="cartBasket">
                                          <i aria-hidden="true" title="Add to Cart" class="fas fa-shopping-cart"></i>
                                      </button>
                                  </div>
                                </div>
                                <p class="pt-3"><a href="#">SKU number : 1012</a></p>
                                <p><a href="#">Categories: Name</a></p>
                                <p><a href="#">Radler source</a></p>
                              </div>
                            </div>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
      <!-- QuickViewModal -->
      <!--WishList Modal  -->
        <div class="modal fade" id="WishListModal" tabindex="-1" aria-labelledby="WishListModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="WishListModalLabel">Product Add in Wishlist</h5>
                  <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  </div>
                  <div class="modal-body m-auto">
                   <button type="button" class="ViewWishlistButton">View Wishlist</button>
                  </div>
              </div>
          </div>
        </div>
      <!--WishList Modal  -->
      <!-- CompareModal -->
        <div class="modal fade" id="CompareModal" tabindex="-1" aria-labelledby="CompareModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="CompareModalLabel">Modal title</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <table class="m-auto container">
                            <tbody>
                                <tr class="TableRow">
                                    <th class="TableHeading"></th>
                                    <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                    <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                    <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                    <td class="TableData"><a href="#">Remove<i class="far fa-window-close"></i></a></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading"></th>
                                    <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                    <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                    <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                    <td class="TableDataImage"><img src="{{ asset('assets/images/product-category/beers/Beer mix drinks/Augustinians bright.jpg') }}"/></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">TITLE</th>
                                    <td class="TableData">BioZisch Mate</td>
                                    <td class="TableData">Augustinians bright</td>
                                    <td class="TableData">Krombacher Radler</td>
                                    <td class="TableData">Bitburger Pils</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">Price</th>
                                    <td class="TableData">€ 12.40</td>
                                    <td class="TableData">€ 19.50</td>
                                    <td class="TableData">€ 16.60</td>
                                    <td class="TableData">€ 15.60</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">ADD TO CART</th>
                                    <td class="TableData"><button>Add to Cart</button></td>
                                    <td class="TableData"><button>Add to Cart</button></td>
                                    <td class="TableData"><button>Add to Cart</button></td>
                                    <td class="TableData"><button>Add to Cart</button></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">DESCRIPTION</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">SKU</th>
                                    <td class="TableData">1022</td>
                                    <td class="TableData">1508</td>
                                    <td class="TableData">1140</td>
                                    <td class="TableData">1104</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">AVAILABILITY</th>
                                    <td class="TableData">In stock</td>
                                    <td class="TableData">In stock</td>
                                    <td class="TableData">In stock</td>
                                    <td class="TableData">In stock</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading" >WEIGHT</th>
                                    <td class="TableData">-</td>
                                    <td class="TableData">-</td>
                                    <td class="TableData">-</td>
                                    <td class="TableData">-</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">DIMENSIONS</th>
                                    <td class="TableData">n / A</td>
                                    <td class="TableData">n / A</td>
                                    <td class="TableData">n / A</td>
                                    <td class="TableData">n / A</td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">MEASURING UNIT</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">SPARKLING</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">FIRE</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">BOTTLE TYPE</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th  class="TableHeading">BOTTLE SIZE</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading">DEGREE OF SWEETNESS</th>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                    <td class="TableData"></td>
                                </tr>
                                <tr class="TableRow">
                                    <th class="TableHeading"></th>
                                    <td class="TableData">€ 12.40</td>
                                    <td class="TableData">€ 19.50</td>
                                    <td class="TableData">€16.60</td>
                                    <td class="TableData">€ 15.60</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- CompareModel -->
    <!-- Modal Area -->
<x-footer-component></x-footer-component>
